import React from 'react';

const Task = ({ task, ...props }) => {
    return (
        <div className="task">
            <p>{task.Title}</p>
        </div>
    )
}

export default Task;